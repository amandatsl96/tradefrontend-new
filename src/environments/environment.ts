// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const URLs = {
  dataService: 'http://ip172-18-0-127-bthl225im9m000drm3f0-8333.direct.labs.play-with-docker.com',
  tradeService: 'http://ip172-18-0-127-bthl225im9m000drm3f0-8080.direct.labs.play-with-docker.com',
  userService: 'http://ip172-18-0-127-bthl225im9m000drm3f0-8222.direct.labs.play-with-docker.com',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
