import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogbuttonComponent } from './logbutton.component';

describe('LogbuttonComponent', () => {
  let component: LogbuttonComponent;
  let fixture: ComponentFixture<LogbuttonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogbuttonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogbuttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
