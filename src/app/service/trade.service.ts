import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {URLs} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Trade} from '../model/trade';
import { AuthenticationService } from '../authentication-service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  private url = URLs.tradeService + '/createTrade';
  constructor(private httpClient: HttpClient, private as : AuthenticationService) {}

  createTrade(trade: Trade): Observable<object>{
    const headers = this.as.authorisation;
    const options = { headers };
    // const params = { params: new HttpParams(trade) };
    const url = `${this.url}?ticker=${trade.ticker}&quantity=${trade.quantity}&type=${trade.type}`;
    return this.httpClient.post(url, {}, options);
  }
}



