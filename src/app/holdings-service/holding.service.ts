import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Holding } from './holding';
import { URLs } from '../../environments/environment';
import { AuthenticationService } from '../authentication-service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HoldingService {

  private readonly url : string = URLs.tradeService + '/getHoldings';

  constructor(private httpClient: HttpClient, private authentication : AuthenticationService) { }

  getHoldings() : Observable<Array<Holding>> {
    let holdings = this.httpClient.get<Array<Holding>> (
      this.url, {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return holdings;
  }

  getDummy() : Holding[] {
    let dummies = new Array<Holding>();
    dummies.push(new Holding('AAPL', 'Apple', 5, 1));
    dummies.push(new Holding('MSFT', 'Microsoft', 3, 1));
    dummies.push(new Holding('C', 'Citi', 2, 1));
    return dummies;
  }

  getHoldingsByTicker(ticker : string) : Observable<Array<Holding>> {
    let holdings = this.httpClient.get<Array<Holding>> (
      this.url + '?' + ticker, {headers: this.authentication.authorisation, responseType: 'json'}
    );
    return holdings;
  }

}
